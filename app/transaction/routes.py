from aiohttp_swagger3 import SwaggerDocs

from app.transaction.views import routes


def setup_routes(swagger: SwaggerDocs):
    swagger.add_routes(routes)
