from pydantic import BaseModel, Field


class TransactionMetaSchema(BaseModel):
    transaction_result: str = Field(
        default="tesSUCCESS", alias="TransactionResult", const=True
    )


class TransactionTxSchema(BaseModel):
    account: str = Field(alias="Account")
    amount: float = Field(alias="Amount")
    destination: str = Field(alias="Destination")
    destination_tag: int = Field(alias="DestinationTag")
    fee: str = Field(alias="Fee")
    hash: str
    ledger_index: int

    class Config:
        orm_mode = True


class TransactionSchema(BaseModel):
    account: str
    amount: float
    destination: str
    destination_tag: int
    fee: str
    hash: str
    ledger_index: int

    class Config:
        orm_mode = True


class RawTransactionSchema(BaseModel):
    meta: TransactionMetaSchema
    tx: TransactionTxSchema

    def output(self) -> dict:
        return {**self.tx.dict()}


class SubscriptionTransactionSchema(BaseModel):
    account: str = Field(alias="Account")
    amount: float = Field(alias="Amount")
    destination: str = Field(alias="Destination")
    destination_tag: int = Field(alias="DestinationTag")
    fee: str = Field(alias="Fee")
    hash: str

    class Config:
        orm_mode = True


class SubscriptionRawTransactionSchema(BaseModel):
    ledger_index: int
    meta: TransactionMetaSchema
    transaction: SubscriptionTransactionSchema

    def output(self) -> dict:
        return {"ledger_index": self.ledger_index, **self.transaction.dict()}
