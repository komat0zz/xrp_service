from app.database import db


class Transaction(db.Model):
    __tablename__ = "transactions"

    account = db.Column(db.String, nullable=False)
    amount = db.Column(db.Float, nullable=False)
    destination = db.Column(db.String, nullable=False)
    destination_tag = db.Column(db.Integer, nullable=False)
    fee = db.Column(db.String, nullable=False)
    hash = db.Column(db.String, nullable=False, primary_key=True)
    ledger_index = db.Column(db.Integer, nullable=False)

    _idx1 = db.Index("transaction_idx_account", "account")
    _idx2 = db.Index("transaction_idx_destination", "destination")
    _idx3 = db.Index("transaction_idx_amount", "amount")
