from aiohttp import web

from app.database.utils import ModelQueryWhereClause
from app.transaction.models import Transaction
from app.transaction.schema import TransactionSchema


class TransactionHandler:
    model = Transaction
    query_filter = ModelQueryWhereClause(model)
    schema = TransactionSchema
    order_by = model.hash

    async def handle_get(self, request):
        """
        ---
        parameters:
          - name: hash
            in: path
            required: true
            schema:
              type: string
        responses:
          '200':
            description: OK.
        """
        primary_key = request.match_info["hash"]
        transaction = await self.model.get(primary_key)
        if not transaction:
            return web.HTTPNotFound()
        return web.json_response(data=self.schema.from_orm(transaction).dict())

    async def handle_list(self, request):
        """
        ---
        parameters:
          - name: account
            in: query
            required: false
            schema:
              type: string
          - name: amount
            in: query
            required: false
            schema:
              type: number
              format: float
          - name: destination
            in: query
            required: false
            schema:
              type: string
          - name: destination_tag
            in: query
            required: false
            schema:
              type: integer
          - name: fee
            in: query
            required: false
            schema:
              type: string
          - name: hash
            in: query
            required: false
            schema:
              type: string
          - name: ledger_index
            in: query
            required: false
            schema:
              type: integer
        responses:
          '200':
            description: OK.
        """
        where_clause = self.query_filter.where(request.rel_url.query)
        items = (
            await self.model.query.where(where_clause)
            .order_by(self.order_by.desc())
            .gino.all()
        )
        result = [self.schema.from_orm(item).dict() for item in items]
        return web.json_response(data={"count": len(result), "result": result})


handler = TransactionHandler()

routes = [
    web.get("/api/v1/transaction/", handler.handle_list, allow_head=False),
    web.get("/api/v1/transaction/{hash}/", handler.handle_get, allow_head=False),
]
