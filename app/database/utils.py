from abc import ABC, abstractmethod
from typing import Type, Any

from pydantic import BaseModel
from sqlalchemy import and_


class AbstractQueryWhereClause(ABC):
    @abstractmethod
    def where(self):
        ...


class ModelQueryWhereClause(ABC):
    def __init__(self, model: Type[BaseModel]):
        self.model = model

    def _get_field_type(self, field: str) -> type:
        return getattr(self.model, field).type.python_type

    def _set_param_type(self, field, value) -> Any:
        python_type = self._get_field_type(field)
        return python_type(value)

    def where(self, query: dict):
        where_clause = []
        for field_name, value in query.items():
            field = getattr(self.model, field_name, None)
            if field is not None:
                value = query[field_name]
                where_clause.append(field == self._set_param_type(field_name, value))
        where_clause += [True, True]
        return and_(*where_clause)
