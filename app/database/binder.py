from abc import ABC, abstractmethod

from aiohttp.web_app import Application
from gino import Gino


class AbstractBinder(ABC):
    @abstractmethod
    def _on_startup(self, *args, **kwargs) -> None:
        ...

    @abstractmethod
    def _on_cleanup(self, *args, **kwargs) -> None:
        ...


class PostgresBinder(AbstractBinder):
    def __init__(self, application: Application, database: "Gino") -> None:
        self.config = None
        self.application = application
        self.database = database
        application.on_startup.append(self._on_startup)
        application.on_cleanup.append(self._on_cleanup)

    async def _on_startup(self, application: Application) -> None:
        self.config = self.application["config"]
        await self.database.set_bind(self.config["DATABASE_URL"])

    async def _on_cleanup(self, _: Application) -> None:
        if self.database is not None:
            await self.database.pop_bind().close()
