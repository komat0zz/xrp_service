from aiohttp import web

from app.database import db
from app.database.binder import PostgresBinder
from app.transaction.routes import setup_routes as setup_transaction_routes
from app.settings import config
from aiohttp_swagger3 import SwaggerDocs, SwaggerUiSettings, SwaggerInfo


async def application():
    app = web.Application()
    app["config"] = config
    PostgresBinder(app, db)
    swagger = SwaggerDocs(
        app,
        swagger_ui_settings=SwaggerUiSettings(path="/docs/"),
        info=SwaggerInfo(title="OpenAPI3", version="1.0.0"),
    )
    setup_transaction_routes(swagger)
    return app
