#!/bin/sh

set -e
set -o nounset

set -e
set -o nounset

if [ "$1" = 'subscriber' ]; then
  echo Starting Transactions Subscriber
  python -m alembic upgrade head
  python /opt/transaction-service/subscriber.py

elif [ "$1" = 'api' ]; then
  echo Starting Transactions API
  python -m alembic upgrade head
  exec gunicorn service:application \
        --name api \
        --bind 0.0.0.0:8000 \
        --worker-class aiohttp.GunicornWebWorker \
        --workers=$WORKERS \
        --log-level=$LOGLEVEL \
        --timeout=$TIMEOUT \
        --log-file=- \
        --access-logfile=-
else
  exec "$@"
fi
