"""initial

Revision ID: 1d0a1ce2352e
Revises: 
Create Date: 2022-04-16 20:10:04.818553

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "1d0a1ce2352e"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "transactions",
        sa.Column("account", sa.String(), nullable=False),
        sa.Column("amount", sa.Float(), nullable=False),
        sa.Column("destination", sa.String(), nullable=False),
        sa.Column("destination_tag", sa.Integer(), nullable=False),
        sa.Column("fee", sa.String(), nullable=False),
        sa.Column("hash", sa.String(), nullable=False),
        sa.Column("ledger_index", sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint("hash"),
    )
    op.create_index(
        "transaction_idx_account", "transactions", ["account"], unique=False
    )
    op.create_index("transaction_idx_amount", "transactions", ["amount"], unique=False)
    op.create_index(
        "transaction_idx_destination", "transactions", ["destination"], unique=False
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index("transaction_idx_destination", table_name="transactions")
    op.drop_index("transaction_idx_amount", table_name="transactions")
    op.drop_index("transaction_idx_account", table_name="transactions")
    op.drop_table("transactions")
    # ### end Alembic commands ###
